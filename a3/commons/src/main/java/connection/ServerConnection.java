package connection;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class ServerConnection {
    private final static String URI = "amqp://vnjikrau:a5HRyFlEJJlwPRAo6YFPJAj9WecGszqf@flamingo.rmq.cloudamqp.com/vnjikrau";

    public ServerConnection() {
    }

    public Connection createConnection() {

        Connection connection = null;
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUri(URI);
            connection = factory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
