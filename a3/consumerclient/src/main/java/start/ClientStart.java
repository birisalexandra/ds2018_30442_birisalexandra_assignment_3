package start;

import start.consumer.EmailConsumer;
import start.consumer.FileConsumer;

import java.io.IOException;

public class ClientStart {

    public static void main(String[] args) throws IOException {
        FileConsumer fileConsumer = new FileConsumer();
        fileConsumer.createTextFile();

        EmailConsumer emailConsumer = new EmailConsumer();
        emailConsumer.notifySubscribers();
    }
}
