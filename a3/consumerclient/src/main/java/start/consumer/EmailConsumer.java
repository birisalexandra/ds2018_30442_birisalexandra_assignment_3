package start.consumer;

import com.google.gson.Gson;
import com.rabbitmq.client.*;
import connection.ServerConnection;
import entities.DVD;
import start.util.MailNotification;

import java.io.IOException;

public class EmailConsumer {

    public EmailConsumer() {
    }

    public void notifySubscribers() throws IOException {
        ServerConnection serverConnection = new ServerConnection();
        Connection connection = serverConnection.createConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare("Exchange", "fanout");
        String queueEmail = channel.queueDeclare().getQueue();
        channel.queueBind(queueEmail, "Exchange", "");

        Consumer consumer = new DefaultConsumer(channel) {

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                Gson gson = new Gson();
                DVD dvd = gson.fromJson(message, DVD.class);
                MailNotification notification = new MailNotification();
                notification.notifySubscribers(dvd.toString());
            }
        };
        channel.basicConsume(queueEmail, true, consumer);
    }
}
