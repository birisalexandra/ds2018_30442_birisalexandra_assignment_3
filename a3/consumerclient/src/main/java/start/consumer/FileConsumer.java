package start.consumer;

import com.google.gson.Gson;
import com.rabbitmq.client.*;
import connection.ServerConnection;
import entities.DVD;

import java.io.FileWriter;
import java.io.IOException;

public class FileConsumer {

    public FileConsumer() {
    }

    public void createTextFile() throws IOException {
        ServerConnection serverConnection = new ServerConnection();
        Connection connection = serverConnection.createConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare("Exchange", "fanout");
        String queueFile = channel.queueDeclare().getQueue();
        channel.queueBind(queueFile, "Exchange", "");

        Consumer consumer = new DefaultConsumer(channel) {

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                Gson gson = new Gson();
                DVD dvd = gson.fromJson(message, DVD.class);

                FileWriter writer = new FileWriter("fisier.txt", true);
                writer.write("A new DVD was added: \r\n");
                writer.write("Title: " + dvd.getTitle() + "\r\n");
                writer.write("Year: " + dvd.getYear() + "\r\n");
                writer.write("Price: " + dvd.getPrice() + "\r\n");
                writer.close();
            }
        };
        channel.basicConsume(queueFile, true, consumer);
    }
}
