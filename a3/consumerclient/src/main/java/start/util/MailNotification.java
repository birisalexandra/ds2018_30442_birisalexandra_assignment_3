package start.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MailNotification {
    private MailService mailService;
    private List<String> subscribers;

    public MailNotification() {
        mailService = new MailService("alebirisdummy@gmail.com","parola123@");
        subscribers = new ArrayList<String>(Arrays.asList("alebirisdummy@gmail.com"));
    }

    public void notifySubscribers(String mail) {
        for(String subscriber: subscribers) {
            mailService.sendMail(subscriber, "Mail time", mail);
        }
    }
}
