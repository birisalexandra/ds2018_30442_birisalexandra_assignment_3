package controller;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import connection.ServerConnection;
import entities.DVD;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ClientController {

    @FXML
    private Button add;

    @FXML
    private TextField year;

    @FXML
    private TextField title;

    @FXML
    private TextField price;

    @FXML
    private Text message;

    public ClientController() {
    }

    public void handleButtonAdd(javafx.event.ActionEvent event) throws IOException, TimeoutException {
        DVD dvd = new DVD(title.getText(), Integer.parseInt(year.getText()), Double.parseDouble(price.getText()));
        Gson gson = new Gson();

        ServerConnection serverConnection = new ServerConnection();
        Connection connection = serverConnection.createConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare("Exchange", "fanout");
        channel.basicPublish("Exchange", "", null, gson.toJson(dvd).getBytes());

        String queueFile = channel.queueDeclare().getQueue();
        String queueEmail = channel.queueDeclare().getQueue();
        channel.queueBind(queueFile, "Exchange", "");
        channel.queueBind(queueEmail, "Exchange", "");

        channel.close();
        connection.close();

        message.setText("The DVD has been added!");
    }
}
